# 一、系统介绍

## 1.全优学堂项目简介

> **电脑端**：包括课时管理、一键排课、学员管理、课程管理、班级管理、课时管理、校区管理、薪资、小程序门店等。
>
> **机构端小程序**：轻松记上课，查询学员剩余课时，已开课列表，学生信息、员工信息、学员评价等。
>
> **家长端小程序**：微信手机号自动绑定学生，自动接收上课通知，查询报读课程、剩余课时、门店/师资/活动信息展示、报名参与活动、课程续费等。
>
> [全优学堂官网](https://jljiayu.cn/) | [部署文档](https://docs.jljiayu.cn/deploy/) | [功能说明](https://docs.jljiayu.cn/guide/)


> 如果项目能帮到你，欢迎⭐️⭐️⭐️Star⭐️⭐️⭐️
>
> 如有任何疑问，欢迎微信或者提Issues

## 2.客服微信

![客服微信](doc/img/0.png)


## 3.版权声明

全优学堂 遵循 GPL-2.0 开源协议发布，并提供技术交流学习，但**绝不允许修改后和衍生的代码做为闭源的商业软件发布和销售！** 如果需要将本产品在本地进行任何附带商业化性质行为使用，**请联系项目负责人进行商业授权**，以遵守 GPL 协议保证您的正常使用。


## 4.鸣谢

基础管理功能代码参考  [ruoyi-vue](https://gitee.com/zhijiantianya) 重新编写
持久层框架增强工具 [MyBatis-Plus](https://baomidou.com/)
微信Java开发工具包 [WxJava](https://gitee.com/binary/weixin-java-tools)

# 二、在线体验

## 1.PC端

> 体验地址：[https://qyxt.jljiayu.cn](https://qyxt.jljiayu.cn)
>
> **账号**：zuhu2
> 
> **密码**：qweRT12345!@#$%

## 2.小程序端

机构端：全优学堂校管家

家长端：全优学堂家校通

![机构端/家长端小程序](doc/img/1.png)

# 三、相关截图
## 1.PC端

![image.png](doc/img/2.png)
![image.png](doc/img/3.png)
![image.png](doc/img/4.png)
![image.png](doc/img/5.png)
![image.png](doc/img/6.png)
![image.png](doc/img/7.png)
![image.png](doc/img/8.png)
![image.png](doc/img/9.png)

## 2.机构端小程序

![image.png](doc/img/10.png)![image.png](doc/img/11.png)

![image.png](doc/img/12.png)![image.png](doc/img/13.png)

![image.png](doc/img/14.png)![image.png](doc/img/15.png)

## 3.家长端小程序

![image.png](doc/img/16.jpg)![image.png](doc/img/17.jpg)![image.png](doc/img/18.png)

# 四、技术选型

## 1.后端

- spring boot 2.1.10.RELEASE
- spring security
- spring-security-jwt
- spring-security-oauth2
- [MyBatis-Plus](https://baomidou.com/)
- [MyBatis-Plus](https://baomidou.com/) 提供的多租户能力
- lombok
- easyexcel
- [WxJava](https://gitee.com/binary/weixin-java-tools)
- redis
- freemarker
- 内存缓存 caffeine

## 2.前端


| 框架                                                                           | 说明        | 版本     |
|------------------------------------------------------------------------------|-----------|--------|
| [Vue](https://cn.vuejs.org/index.html)                                       | 前端框架      | 2.6.10 |
| [Vue Element Admin](https://panjiachen.github.io/vue-element-admin-site/zh/) | 管理后台页面脚手架 | -      |


## 3.项目模块结构

| 项目                | 说明                               |
|-------------------|----------------------------------|
| base-common       | 公共依赖                             |
| base-config-redis | redis相关配置                        |
| base-oauth-server | oAuth2 认证服务                      |
| base-service      | 基础服务，包括数据库操作mapper、实体类、及service等 |
| business-server   | oAuth2 资源服务                      |
