package cn.xluobo.business.stock.goods.repo.mapper;

import cn.xluobo.business.stock.goods.repo.model.StockGoodsProperty;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 商品属性 Mapper 接口
 * </p>
 *
 * @author zhangby
 * @since 2021-01-12 07:46:39
 */
public interface StockGoodsPropertyMapper extends com.baomidou.mybatisplus.core.mapper.BaseMapper<StockGoodsProperty> {

}
