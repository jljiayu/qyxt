package cn.xluobo.business.mk.discount.repo.mapper;

import cn.xluobo.business.mk.discount.repo.model.MkDiscountDept;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 活动校区 Mapper 接口
 * </p>
 *
 * @author zhangby
 * @since 2020-08-21
 */
public interface MkDiscountDeptMapper extends BaseMapper<MkDiscountDept> {

}
