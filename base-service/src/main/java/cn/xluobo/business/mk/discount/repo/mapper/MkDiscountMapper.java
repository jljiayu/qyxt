package cn.xluobo.business.mk.discount.repo.mapper;

import cn.xluobo.business.mk.discount.repo.model.MkDiscount;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 优惠配置 Mapper 接口
 * </p>
 *
 * @author zhangby
 * @since 2020-08-21
 */
public interface MkDiscountMapper extends BaseMapper<MkDiscount> {

}
