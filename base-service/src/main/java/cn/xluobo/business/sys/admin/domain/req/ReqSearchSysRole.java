package cn.xluobo.business.sys.admin.domain.req;

import cn.xluobo.business.sys.admin.repo.model.SysRole;

import java.io.Serializable;

/**
 * @author ：zhangbaoyu
 * @date ：Created in 2020-01-14 17:30
 */
public class ReqSearchSysRole extends SysRole implements Serializable {
}
