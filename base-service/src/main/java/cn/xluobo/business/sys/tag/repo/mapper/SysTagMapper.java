package cn.xluobo.business.sys.tag.repo.mapper;

import cn.xluobo.business.sys.tag.repo.model.SysTag;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 标签 Mapper 接口
 * </p>
 *
 * @author zhangby
 * @since 2020-09-03 03:08:13
 */
public interface SysTagMapper extends com.baomidou.mybatisplus.core.mapper.BaseMapper<SysTag> {

}
