package cn.xluobo.business.sys.admin.service;

import cn.xluobo.business.sys.admin.repo.model.SysUserRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户角色表 服务类
 * </p>
 *
 * @author zhangby
 * @since 2020-01-12
 */
public interface ISysUserRoleService extends IService<SysUserRole> {

}
