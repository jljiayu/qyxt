package cn.xluobo.business.sys.admin.service;

import cn.xluobo.business.sys.admin.repo.model.SysFile;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 附件表 服务类
 * </p>
 *
 * @author zhangby
 * @since 2020-03-01 10:09:44
 */
public interface ISysFileService extends IService<SysFile> {

}
