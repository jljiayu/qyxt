package cn.xluobo.business.sc.student.service.impl;

import cn.xluobo.business.sc.student.repo.model.ScStudentAccountLog;
import cn.xluobo.business.sc.student.repo.mapper.ScStudentAccountLogMapper;
import cn.xluobo.business.sc.student.service.IScStudentAccountLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 账户变更日志 服务实现类
 * </p>
 *
 * @author zhangby
 * @since 2020-08-21
 */
@Service
public class ScStudentAccountLogServiceImpl extends ServiceImpl<ScStudentAccountLogMapper, ScStudentAccountLog> implements IScStudentAccountLogService {

}
