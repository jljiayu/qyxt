package cn.xluobo.business.sc.student.repo.mapper;

import cn.xluobo.business.sc.student.repo.model.ScStudentAccountLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 账户变更日志 Mapper 接口
 * </p>
 *
 * @author zhangby
 * @since 2020-08-21
 */
public interface ScStudentAccountLogMapper extends BaseMapper<ScStudentAccountLog> {

}
