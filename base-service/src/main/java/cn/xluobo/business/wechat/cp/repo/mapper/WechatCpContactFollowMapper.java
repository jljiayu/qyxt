package cn.xluobo.business.wechat.cp.repo.mapper;

import cn.xluobo.business.wechat.cp.repo.model.WechatCpContactFollow;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 外部联系人对应大员工信息 Mapper 接口
 * </p>
 *
 * @author xluobo
 * @since 2024-01-25 05:53:25
 */
public interface WechatCpContactFollowMapper extends com.baomidou.mybatisplus.core.mapper.BaseMapper<WechatCpContactFollow> {

}
