package cn.xluobo.business.wechat.cp.repo.mapper;

import cn.xluobo.business.wechat.cp.repo.model.WechatCpContactFollowTag;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 员工给外部联系人添加的标签 Mapper 接口
 * </p>
 *
 * @author xluobo
 * @since 2024-01-25 10:46:17
 */
public interface WechatCpContactFollowTagMapper extends com.baomidou.mybatisplus.core.mapper.BaseMapper<WechatCpContactFollowTag> {

}
