package cn.xluobo.business.wechat.cp.service;

import cn.xluobo.business.wechat.cp.repo.model.WechatCpGroupTag;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 企业微信标签 服务类
 * </p>
 *
 * @author xluobo
 * @since 2024-01-25 05:55:40
 */
public interface IWechatCpGroupTagService extends com.baomidou.mybatisplus.extension.service.IService<WechatCpGroupTag> {

}
