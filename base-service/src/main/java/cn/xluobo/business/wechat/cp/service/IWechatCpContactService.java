package cn.xluobo.business.wechat.cp.service;

import cn.xluobo.business.wechat.cp.repo.model.WechatCpContact;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 企业微信客户信息 服务类
 * </p>
 *
 * @author xluobo
 * @since 2024-01-25 05:53:20
 */
public interface IWechatCpContactService extends com.baomidou.mybatisplus.extension.service.IService<WechatCpContact> {

}
