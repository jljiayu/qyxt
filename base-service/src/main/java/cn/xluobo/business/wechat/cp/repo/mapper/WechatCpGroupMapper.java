package cn.xluobo.business.wechat.cp.repo.mapper;

import cn.xluobo.business.wechat.cp.repo.model.WechatCpGroup;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 企业微信标签组 Mapper 接口
 * </p>
 *
 * @author xluobo
 * @since 2024-01-25 05:53:33
 */
public interface WechatCpGroupMapper extends com.baomidou.mybatisplus.core.mapper.BaseMapper<WechatCpGroup> {

}
