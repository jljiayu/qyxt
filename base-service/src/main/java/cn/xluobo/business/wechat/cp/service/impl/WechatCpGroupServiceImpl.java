package cn.xluobo.business.wechat.cp.service.impl;

import cn.xluobo.business.wechat.cp.repo.model.WechatCpGroup;
import cn.xluobo.business.wechat.cp.repo.mapper.WechatCpGroupMapper;
import cn.xluobo.business.wechat.cp.service.IWechatCpGroupService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 企业微信标签组 服务实现类
 * </p>
 *
 * @author xluobo
 * @since 2024-01-25 05:53:33
 */
@Service
public class WechatCpGroupServiceImpl extends ServiceImpl<WechatCpGroupMapper, WechatCpGroup> implements IWechatCpGroupService {

}
