package cn.xluobo;

import com.alibaba.fastjson.JSON;
import org.junit.Test;
import org.springframework.security.jwt.Jwt;
import org.springframework.security.jwt.JwtHelper;
import org.springframework.security.oauth2.common.util.JsonParser;
import org.springframework.security.oauth2.common.util.JsonParserFactory;

import java.util.Map;

/**
 * @author ：zhangbaoyu
 * @date ：Created in 2020/7/4 14:04
 */
public class JwtConverterTest {

    @Test
    public void a(){
        JsonParser objectMapper = JsonParserFactory.create();
        Jwt jwt = JwtHelper.decode("eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsiYnVzaW5lc3Mtc2VydmVyIl0sInVzZXJfbmFtZSI6ImFkbWluIiwic2NvcGUiOlsiYWxsIl0sIm5hbWUiOiLns7vnu5_nrqHnkIblkZgiLCJleHAiOjE1OTM4NDM3MzYsInVzZXJJZCI6IjExOTI0NTM4MDc1NzM5NDIyNzMiLCJqdGkiOiI4MTdhNDU1Yy05MGQ1LTRhMzgtYTM3YS1iZmM3NWE3NjAzMjgiLCJjbGllbnRfaWQiOiJidXNpbmVzcy1zZXJ2ZXIiLCJ1c2VybmFtZSI6ImFkbWluIn0.bnhx1YPr4cNUzr7osIYTcvddIY5246mabg1qiA2h1F6aPrAVPCrlyHOVaE_LC3H3D4lkj9KRYh7lZEZa3ekyzauaSC4bBKmbaz-v3Amzh3_-QFxARX5ks8v_j_bBvBeDPj9L2uwHuZmooiHM1Es4Tlklyxb1xXMnCE2VCsg6owsTzHk0t_BYrD6c6E9xEKHzIqhXWGY6M-PPDkfa38eav6jWE6T0LkreHZBFovs0iIZw8ASqv9wIf0p1mWIIWpNy8AQld983Ci-V7tjBPIHc-DaUzdN4LNCzsj5m2aSTx7F-2MH8TZrwVngjO7emMqbT8FKMzJ6CsC9qtBHTtJle0Q");
        String claims = jwt.getClaims();
        Map<String, Object> stringObjectMap = objectMapper.parseMap(claims);
        System.out.println(JSON.toJSONString(stringObjectMap));
    }
}
