package cn.xluobo;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Date;
import java.util.List;

/**
 * @author ：zhangbaoyu
 * @date ：Created in 2020-01-21 22:35
 */
public class TempTest {

    @Test
    public void t() throws IOException {
        createFile("/Users/zhangbaoyu/IdeaProjects/zhangby/qyxt/a-temp/cn/xluobo/business/sys/dept/controller");
    }

    private void createFile(String path) throws IOException {
        if (StringUtils.isNotEmpty(path)) {
            File file = new File(path);
            if (!file.getParentFile().exists()) {
                file.getParentFile().mkdirs();
            }
            file.createNewFile();
        }
    }

    @Test
    public void test1() {
        Duration.parse("PT30M");
    }

    @Test
    public void test2() {
        String json = "{\"2021\":false}";
        JSONObject object = JSON.parseObject(json);
        Object o = object.get("2021");
        System.out.println(o instanceof Boolean);
    }

    @Test
    public void test3() {
        String json = "{\"2020\":{\"0101\":2,\"0124\":1,\"0125\":2,\"0126\":2,\"0127\":2,\"0128\":1,\"0129\":1,\"0130\":1,\"0131\":1,\"0201\":1,\"0202\":1,\"0404\":2,\"0405\":1,\"0406\":1,\"0501\":2,\"0502\":1,\"0503\":1,\"0504\":1,\"0505\":1,\"0625\":2,\"0626\":1,\"0627\":1,\"1001\":2,\"1002\":2,\"1003\":2,\"1004\":1,\"1005\":1,\"1006\":1,\"1007\":1,\"1008\":1}}";
        JSONObject object = JSON.parseObject(json);
        Object o = object.get("2020");
        System.out.println(o instanceof JSONObject);
    }

    @Test
    public void testA() {
        for(int i=0;i<26;i++) {
            System.out.println((char)(65+i));
        }
    }

    @Test
    public void testFile() throws IOException, ParseException {
        File file = new File("/Users/zhangbaoyu/IdeaProjects/zhangby/qyxt/business-server/src/test/java/cn/xluobo/1600584206476_0");
        FileInputStream fileInputStream = new FileInputStream(file);
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fileInputStream));

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss:SSS");

        long preTime = 0;
        // 上一行
        String preLine = "";

        boolean preMerge = false;
        String line = null;
        while ((line = bufferedReader.readLine()) != null) {

            boolean tag13 = false; // 是否<=13
            boolean tag100 = false; // 是否大于100

            String[] split = line.split("\\^");
            String tag = split[0];
            String time = split[1];

            Date date = simpleDateFormat.parse(time);


//            System.out.println(tag);
//            System.out.println(simpleDateFormat.format(date));
            if (preTime == 0) {
//                System.out.println(line);
            } else {
                long diff = date.getTime() - preTime;
                if (diff <= 13) {
                    // 两个U之间小于13毫秒的，将两个U合并成一行，并转成字符K
                    tag13 = true;
                } else if (diff > 100) {
                    // 间隔大于100毫秒的，两个U之间塞进去一个字符N
                    tag100 = true;
                }
//                System.out.println(line + " " + diff);
            }




            if (tag13) {
                // 两个U之间小于13毫秒的，将两个U合并成一行，并转成字符K
                System.out.println("K 两行合并一行,分别为" + preLine + "，" + line);
                preMerge = true;
            } else if(tag100) {
                // 间隔大于100毫秒的，两个U之间塞进去一个字符N
                if(preMerge) {
                    System.out.println("N");
                } else {
                    System.out.println(preLine);
                    System.out.println("N");
                }
                preMerge = false;
            } else {
                if(preMerge){

                } else {
                    System.out.println(preLine);
                }
                preMerge = false;
            }

            preTime = date.getTime();
            preLine = line;
        }

        System.out.println(preLine);

        //close
        fileInputStream.close();
        bufferedReader.close();
    }
}
